<?php
/**
 * Created by PhpStorm.
 * User: Dima
 */

namespace CoreBundle\Controller;


use CoreBundle\Entity\Book;
use CoreBundle\Form\BookFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * @Route(path="/search")
 */
class SearchController extends Controller
{
    /**
     * @Route(path="/", name="search")
     * @Template()
     */
    public function searchAction(Request $request)
    {
        /** @var  $qb */
        $qb = $this->get('doctrine.orm.default_entity_manager')->getRepository(Book::class)->createQueryBuilder('b');

        $form = $this->createForm(new BookFilterType());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($query = $form->get('query')->getData()) {
                $qb->addSelect("MATCH_AGAINST (b.title, :searchterm ) as HIDDEN score")
                    ->add('where', 'MATCH_AGAINST(b.title, :searchterm) > 0.0')
                    ->setParameter('searchterm', $form->get('query')->getData())
                    ->orderBy('score', 'desc');
            }

            if ($author = $form->get('author')->getData()) {
                $qb->andWhere('b.author = :author')->setParameter('author', $author);
            }
            if ($genre = $form->get('genre')->getData()) {
                $qb->andWhere(':genre MEMBER OF b.genres')->setParameter('genre', $genre);
            }

        }

        return [
            'books' => $qb->getQuery()->getResult(),
            'form' => $form->createView(),
        ];
    }
}