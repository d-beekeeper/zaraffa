<?php
/**
 * Created by PhpStorm.
 * User: Dima
 */

namespace CoreBundle\Controller;


use CoreBundle\Entity\Author;
use CoreBundle\Entity\Book;
use CoreBundle\Entity\Genre;
use CoreBundle\Form\AuthorType;
use CoreBundle\Form\BookType;
use CoreBundle\Form\GenreType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route(path="/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends Controller
{
    /**
     * @Route(path="/", name="admin")
     * @Template()
     */
    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();

        return [
            'authors' => $em->getRepository(Author::class)->findAll(),
            'genres' => $em->getRepository(Genre::class)->findAll(),
            'books' => $em->getRepository(Book::class)->findAll(),
        ];
    }

    /**
     * @Route(path="/author/add", name="admin_author_add")
     * @Route(path="/author/{author}", name="admin_author_edit", requirements={"author" : "\d+"})
     * @Method({"GET","POST"})
     * @Template()
     */
    public function editAuthor(Request $request,Author $author = null)
    {
        $author = ($author)? $author : new Author();
        $form = $this->createForm(new AuthorType(), $author);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($author);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route(path="/author/{author}/delete", name="admin_author_delete", requirements={"author" : "\d+"})
     * @Method("POST")
     */
    public function removeAuthor(Author $author)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($author);
        $em->flush();
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route(path="/genre/add", name="admin_genre_add")
     * @Route(path="/genre/{genre}", name="admin_genre_edit", requirements={"genre" : "\d+"})
     * @Method({"GET","POST"})
     * @Template()
     */
    public function editGenre(Request $request, Genre $genre = null)
    {
        $genre = ($genre)? $genre : new Genre();
        $form = $this->createForm(new GenreType(), $genre);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($genre);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route(path="/genre/{genre}/delete", name="admin_genre_delete", requirements={"genre" : "\d+"})
     * @Method("POST")
     */
    public function removeGenre(Genre $genre)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($genre);
        $em->flush();
        return $this->redirectToRoute('admin');
    }


    /**
     * @Route(path="/book/add", name="admin_book_add")
     * @Route(path="/book/{book}", name="admin_book_edit", requirements={"book" : "\d+"})
     * @Method({"GET","POST"})
     * @Template()
     */
    public function editBook(Request $request, Book $book = null)
    {
        $book = ($book)? $book : new Book();
        $form = $this->createForm(new BookType(), $book);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route(path="/book/{book}/delete", name="admin_book_delete", requirements={"book" : "\d+"})
     * @Method("POST")
     */
    public function removeBook(Book $book)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();
        return $this->redirectToRoute('admin');
    }


}