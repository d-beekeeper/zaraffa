<?php

namespace CoreBundle\Controller;

use CoreBundle\Entity\User;
use CoreBundle\Form\ResetType;
use CoreBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class IndexController
 * @package CoreBundle\Controller
 *
 * @Route(path="/")
 */
class IndexController extends Controller
{
    /**
     * @Route(path="/", name="")
     * @Template()
     * @param $name
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route(path="/login", name="login")
     * @Template()
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return [
            'last_username' => $lastUsername,
            'error'         => $error,
        ];
    }

    /**
     * @Route(path="/register", name="register")
     * @Template()
     */
    public function registerAction(Request $request)
    {

        $user = new User();
        $form = $this->createForm(new UserType(), $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route(path="/reset", name="forgot-password")
     * @Template()
     */
    public function forgotPasswordAction(Request $request)
    {
        $form = $this->createForm(new ResetType());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $form->get('password')->getData())) {
                $form->get('password')->addError(new FormError('Wrong password'));
            } else {
                $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData()));
                $this->getDoctrine()->getManager()->flush();
                return $this->redirect('/');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
