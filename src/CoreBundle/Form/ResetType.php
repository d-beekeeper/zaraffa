<?php
/**
 * Created by PhpStorm.
 * User: Dima
 */

namespace CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ResetType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('password', 'password', ['max_length' => 64,'constraints' => [
            new Assert\NotBlank(),
            new Assert\Length(['max'=>254]),
        ]])
            ->add('newPassword', 'repeated', [
                'type' => 'password',
                'first_options'  => array('label' => 'New Password'),
                'second_options' => array('label' => 'Repeat Password'),
                'invalid_message' => 'Values mismatch',
                'max_length' => 254,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max'=>254]),
                ]
            ])
            ->add('Reset','submit')
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'reset_password';
    }
}