<?php
/**
 * Created by PhpStorm.
 * User: Dima
 */

namespace CoreBundle\Form;


use CoreBundle\Entity\Author;
use CoreBundle\Entity\Genre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class BookFilterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', 'entity', [
                'class' => Author::class,
                'empty_value' => 'All',
                'required' => false,
            ])
            ->add('genre', 'entity', [
                'class' => Genre::class,
                'empty_value' => 'All',
                'required' => false,
            ])
            ->add('query', 'text', [
                'label' => 'Search',
                'required' => false,
            ])
            ->add('Search', 'submit')
            ->setMethod('GET')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'book_filter';
    }
}