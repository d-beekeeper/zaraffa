<?php
/**
 * Created by PhpStorm.
 * User: Dima
 */

namespace CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity()
 * @ORM\Table(indexes={@ORM\Index(columns={"title"}, flags={"fulltext"})})
 */
class Book
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=254)
     * @Assert\NotBlank()
     * @Assert\Length(max="254")
     */
    protected $title;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Author", inversedBy="books")
     * @ORM\JoinColumn(fieldName="author_id", nullable=false)
     * @var Author
     */
    protected $author;

    /**
     * @Assert\Count(min="1")
     * @ORM\ManyToMany(targetEntity="CoreBundle\Entity\Genre", inversedBy="books")
     * @ORM\JoinTable(name="books_genre")
     * @var Genre[]|ArrayCollection
     */
    protected $genres;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param \CoreBundle\Entity\Author $author
     * @return Book
     */
    public function setAuthor(\CoreBundle\Entity\Author $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \CoreBundle\Entity\Author 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add genres
     *
     * @param \CoreBundle\Entity\Genre $genres
     * @return Book
     */
    public function addGenre(\CoreBundle\Entity\Genre $genres)
    {
        $this->genres[] = $genres;

        return $this;
    }

    /**
     * Remove genres
     *
     * @param \CoreBundle\Entity\Genre $genres
     */
    public function removeGenre(\CoreBundle\Entity\Genre $genres)
    {
        $this->genres->removeElement($genres);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGenres()
    {
        return $this->genres;
    }
}
