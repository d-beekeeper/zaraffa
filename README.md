### Installation:

1. `git clone https://d-beekeeper@bitbucket.org/d-beekeeper/zaraffa.git`
2. `cd zaraffa`
3. `vagrant up`
4. `http://192.168.56.101`

PS: Build-in admin credentials:

- email: admin
- password: 123